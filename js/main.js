const _ = document;
/* The Info Block */

// Get Player Name
let playerName = prompt('What is Your Name?','Guest'),
    name = _.querySelector('.name strong');

name.innerHTML = playerName




/* Game Cards */

// Variables
let gameBlocks = _.querySelector('.memory-card-block'),
    cardBlocks = Array.from(gameBlocks.children),
    orderRange = [...Array(cardBlocks.length).keys()],
    duration = 1000;

shuffle(orderRange); // Initialize The Shuffle for order number

/*Set order for each card*/

cardBlocks.forEach((cardBlock,index) =>{
    cardBlock.style.order = orderRange[index];
    //Event To The Flip Card
    cardBlock.addEventListener('click',function () {
        flipCard(cardBlock);
    });

});


/**
* @param x this the array
* */

function shuffle(x) {
    let current = x.length,
        temp,
        random;

    while (current > 0){
    random = Math.floor(Math.random() * current);
    current --;
    temp = x[current];
    x[current] = x[random];
    x[random] = temp;
    }
    return x;

}

/**
 * To Flip the card
 * @param selectedCard is the card block
 * */

function flipCard(selectedCard) {
    // Add the flip class
    selectedCard.classList.add('is-flipped');
    // filter the cards which have the class is-flipped
    let allCardsFlipped = cardBlocks.filter(cardFlipped => cardFlipped.classList.contains('is-flipped'));
    if(allCardsFlipped.length === 2){
        stopClicking();
        checkMatched(allCardsFlipped[0],allCardsFlipped[1]);
    }

}


function stopClicking() {
    gameBlocks.classList.add('no-clicking');
    setTimeout(() => {
        gameBlocks.classList.remove('no-clicking');
    },duration);
}


function checkMatched(firstCard,secondCard) {
    // Number of Wrong Steps
    let tring = _.querySelector('.tries span strong');

    if(firstCard.dataset.name === secondCard.dataset.name){
        firstCard.classList.remove('is-flipped');
        secondCard.classList.remove('is-flipped');

        firstCard.classList.add('has-match');
        secondCard.classList.add('has-match');
    }else {
        tring.innerHTML = parseInt(tring.innerHTML) + 1;
        setTimeout(() => {
            firstCard.classList.remove('is-flipped');
            secondCard.classList.remove('is-flipped');
        },duration);
    }
}